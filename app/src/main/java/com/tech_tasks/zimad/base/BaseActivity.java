package com.tech_tasks.zimad.base;

import android.os.Bundle;

import com.tech_tasks.zimad.utils.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;

    @NonNull
    public ViewModelFactory getViewModelFactory() {
        return viewModelFactory;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
    }

    @LayoutRes
    public abstract int getLayoutId();

    @NonNull
    protected <T extends BaseViewModel> T getViewModel(@NonNull Class<T> viewModel) {
        return ViewModelProviders.of(this, getViewModelFactory()).get(viewModel);
    }

}

package com.tech_tasks.zimad.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragment<DB extends ViewDataBinding, VM extends BaseViewModel>
        extends Fragment {

    @Nullable
    private BaseActivity activity;
    private View rootView;
    private DB viewDataBinding;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Nullable
    public BaseActivity getBaseActivity() {
        return activity;
    }

    protected DB getViewDataBinding() {
        return viewDataBinding;
    }

    protected abstract VM getViewModel();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseActivity) {
            activity = (BaseActivity) context;
        }
    }

    @Override
    public void onDetach() {
        activity = null;
        super.onDetach();
    }

    @Override
    public void onPause() {
        compositeDisposable.clear();
        super.onPause();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getViewModel().start();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        viewDataBinding.setVariable(getDataBindingViewModelVariable(), getViewModel());
        viewDataBinding.executePendingBindings();
        rootView = viewDataBinding.getRoot();
        return rootView;
    }

    @LayoutRes
    public abstract int getLayoutId();

    public abstract int getDataBindingViewModelVariable();

}

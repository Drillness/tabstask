package com.tech_tasks.zimad.base;

import java.util.List;


public interface BindableAdapter<T> {

    void setData(List<T> data);
}

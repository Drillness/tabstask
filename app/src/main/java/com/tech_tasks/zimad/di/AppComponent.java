package com.tech_tasks.zimad.di;

import com.tech_tasks.zimad.data.NetworkModule;
import com.tech_tasks.zimad.ui.details.DetailsActivity;
import com.tech_tasks.zimad.ui.details.DetailsFragment;
import com.tech_tasks.zimad.ui.details.DetailsModule;
import com.tech_tasks.zimad.ui.main.AnimalListActivity;
import com.tech_tasks.zimad.ui.main.AnimalListFragment;
import com.tech_tasks.zimad.ui.main.AnimalListModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, AnimalListModule.class, DetailsModule.class})
public interface AppComponent {

    void inject(AnimalListActivity activity);

    void inject(AnimalListFragment fragment);

    void inject(DetailsFragment fragment);

    void inject(DetailsActivity activity);

}

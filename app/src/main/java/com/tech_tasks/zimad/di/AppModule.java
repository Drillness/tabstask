package com.tech_tasks.zimad.di;

import android.app.Application;
import android.content.Context;

import com.tech_tasks.zimad.TestApplication;
import com.tech_tasks.zimad.api.KotApi;
import com.tech_tasks.zimad.utils.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context appContext) {
        context = appContext;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    Application provideApp() {
        return (Application) context;
    }

    @Provides
    @Singleton
    ViewModelFactory provideViewModelFactory(Context context, KotApi api) {
        return new ViewModelFactory((TestApplication) context, api);
    }

}
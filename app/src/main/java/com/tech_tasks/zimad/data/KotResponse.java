package com.tech_tasks.zimad.data;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KotResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<AnimalEntry> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnimalEntry> getData() {
        return data;
    }

    public void setData(List<AnimalEntry> data) {
        this.data = data;
    }

}
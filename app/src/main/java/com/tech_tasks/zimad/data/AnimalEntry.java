package com.tech_tasks.zimad.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnimalEntry implements Parcelable {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("title")
    @Expose
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private AnimalEntry(Parcel in) {
        url = in.readString();
        title = in.readString();
    }

    public static final Creator<AnimalEntry> CREATOR = new Creator<AnimalEntry>() {
        @Override
        public AnimalEntry createFromParcel(Parcel in) {
            return new AnimalEntry(in);
        }

        @Override
        public AnimalEntry[] newArray(int size) {
            return new AnimalEntry[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.title);
    }
}

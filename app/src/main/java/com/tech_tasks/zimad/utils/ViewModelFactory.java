package com.tech_tasks.zimad.utils;

import android.app.Application;

import com.tech_tasks.zimad.api.KotApi;
import com.tech_tasks.zimad.ui.details.DetailsViewModel;
import com.tech_tasks.zimad.ui.main.AnimalListViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

@Singleton
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    @NonNull
    private final Application application;
    @NonNull
    private final KotApi kotApi;

    @Inject
    public ViewModelFactory(@NonNull Application application, @NonNull KotApi kotApi) {
        this.application = application;
        this.kotApi = kotApi;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AnimalListViewModel.class)) {
            return (T) new AnimalListViewModel(kotApi);
        } else if (modelClass.isAssignableFrom(DetailsViewModel.class)) {
            return (T) new DetailsViewModel();
        }

        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }

}

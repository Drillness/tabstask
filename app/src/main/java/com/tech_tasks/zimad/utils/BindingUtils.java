package com.tech_tasks.zimad.utils;

import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tech_tasks.zimad.base.BindableAdapter;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class BindingUtils {

    @BindingAdapter("data")
    @SuppressWarnings("unchecked")
    public static <T> void setItems(RecyclerView recyclerView, List<T> books) {
        if (recyclerView.getAdapter() instanceof BindableAdapter<?>) {
            ((BindableAdapter<T>) recyclerView.getAdapter()).setData(books);
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, @Nullable String imageLink) {
        if (imageLink == null) {
            view.setImageDrawable(view.getResources()
                    .getDrawable(android.R.drawable.star_off));
            return;
        }

        Picasso.get()
                .load(imageLink)
                .into(view, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get()
                                .load(imageLink)
                                .error(android.R.drawable.stat_notify_error)
                                .into(view);
                    }
                });
    }

}

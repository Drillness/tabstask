package com.tech_tasks.zimad;

import android.app.Application;

import com.tech_tasks.zimad.data.NetworkModule;
import com.tech_tasks.zimad.di.AppComponent;
import com.tech_tasks.zimad.di.AppModule;
import com.tech_tasks.zimad.di.DaggerAppComponent;
import com.tech_tasks.zimad.ui.details.DetailsModule;
import com.tech_tasks.zimad.ui.main.AnimalListModule;

import androidx.annotation.NonNull;

public class TestApplication extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildAppComponent();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @NonNull
    private AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .detailsModule(new DetailsModule())
                .animalListModule(new AnimalListModule())
                .build();
    }

}

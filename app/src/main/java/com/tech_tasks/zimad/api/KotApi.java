package com.tech_tasks.zimad.api;

import com.tech_tasks.zimad.data.KotResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface KotApi {

    String URL_BASE = "https://kot3.com";

    @GET
    Observable<KotResponse> get(@Url String query);

}

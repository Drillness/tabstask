package com.tech_tasks.zimad.ui.details;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsModule {

    @Provides
    DetailsFragment provideDetailsFragment() {
        return new DetailsFragment();
    }

}

package com.tech_tasks.zimad.ui.details;

import android.os.Bundle;

import com.tech_tasks.zimad.BR;
import com.tech_tasks.zimad.R;
import com.tech_tasks.zimad.TestApplication;
import com.tech_tasks.zimad.base.BaseFragment;
import com.tech_tasks.zimad.databinding.FragmentDetailsBinding;
import com.tech_tasks.zimad.utils.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class DetailsFragment extends BaseFragment<FragmentDetailsBinding, DetailsViewModel> {

    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TestApplication.getAppComponent().inject(this);
    }

    @Override
    protected DetailsViewModel getViewModel() {
        assert getBaseActivity() != null;
        return ViewModelProviders.of(getBaseActivity(), viewModelFactory)
                .get(DetailsViewModel.class);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_details;
    }

    @Override
    public int getDataBindingViewModelVariable() {
        return BR.viewModel;
    }

}

package com.tech_tasks.zimad.ui.main;

import com.tech_tasks.zimad.BR;
import com.tech_tasks.zimad.api.KotApi;
import com.tech_tasks.zimad.base.BaseViewModel;
import com.tech_tasks.zimad.data.AnimalEntry;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AnimalListViewModel extends BaseViewModel<AnimalListNavigator> {

    @Inject
    List<AnimalEntry> animalEntryList = new ArrayList<>();

    private MutableLiveData<Boolean> finishedLoading = new MutableLiveData<>(false);
    private KotApi api;
    private String request;

    public AnimalListViewModel(KotApi api) {
        this.api = api;
    }

    @Bindable
    public List<AnimalEntry> getAnimalEntryList() {
        return animalEntryList;
    }

    MutableLiveData<Boolean> getLiveData() {
        return finishedLoading;
    }

    void onItemSelected(AnimalEntry animalEntry) {
        getNavigator().openDetails(animalEntry);
    }

    void makeRequest(@NonNull String request) {
        this.request = request;
        finishedLoading.postValue(false);
        getCompositeDisposable()
                .add(api.get(urlFrom(request))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                result -> {
                                    animalEntryList.clear();
                                    animalEntryList.addAll(result.getData());

                                    notifyPropertyChanged(BR.animalEntryList);
                                    finishedLoading.postValue(true);
                                },
                                Throwable::printStackTrace)
                );
    }

    @Override
    public void start() {
        if (request != null)
            makeRequest(request);
    }

    private String urlFrom(String request) {
        return "/xim/api.php?query=" + request;
    }

}

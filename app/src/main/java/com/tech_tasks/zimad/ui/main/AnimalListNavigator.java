package com.tech_tasks.zimad.ui.main;

import com.tech_tasks.zimad.data.AnimalEntry;

public interface AnimalListNavigator {

    void openDetails(AnimalEntry animalEntry);

}

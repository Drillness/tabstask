package com.tech_tasks.zimad.ui.details;

public interface DetailsNavigator {

    void goBackToList();

}

package com.tech_tasks.zimad.ui.main;

import android.os.Bundle;
import android.os.Parcelable;

import com.tech_tasks.zimad.BR;
import com.tech_tasks.zimad.R;
import com.tech_tasks.zimad.TestApplication;
import com.tech_tasks.zimad.base.BaseFragment;
import com.tech_tasks.zimad.databinding.FragmentAnimalListBinding;
import com.tech_tasks.zimad.utils.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

public class AnimalListFragment extends BaseFragment<FragmentAnimalListBinding, AnimalListViewModel> {

    @Inject
    ViewModelFactory viewModelFactory;

    private static final String BUNDLE_RECYCLER_LAYOUT = "BUNDLE_RECYCLER_LAYOUT";
    private Parcelable state;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TestApplication.getAppComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AnimalEntryAdapter adapter = new AnimalEntryAdapter(this);
        getViewDataBinding().rvItems.setAdapter(adapter);
        getViewDataBinding().rvItems.setLayoutManager(new LinearLayoutManager(getContext()));

        // Network request is async, so we need to make sure we restore state after
        // response is received
        getViewModel().getLiveData().observe(this, isChangeCompleted -> {
            if (isChangeCompleted) {
                if (getViewDataBinding().rvItems.getLayoutManager() != null)
                    getViewDataBinding().rvItems.getLayoutManager()
                            .onRestoreInstanceState(state);
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_animal_list;
    }

    protected AnimalListViewModel getViewModel() {
        assert getBaseActivity() != null;
        return ViewModelProviders.of(this, viewModelFactory)
                .get(AnimalListViewModel.class);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if ((savedInstanceState != null)
                && (getViewDataBinding().rvItems.getLayoutManager() != null)) {
            state = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            getViewDataBinding().rvItems.getLayoutManager()
                    .onRestoreInstanceState(state);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getViewDataBinding().rvItems.getLayoutManager() != null) {
            getViewDataBinding().rvItems.getLayoutManager().onRestoreInstanceState(state);
        }
    }

    @Override
    public void onPause() {
        if (getViewDataBinding().rvItems.getLayoutManager() != null) {
            state = getViewDataBinding().rvItems.getLayoutManager().onSaveInstanceState();
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, state);
    }

    public int getDataBindingViewModelVariable() {
        return BR.viewModel;
    }

}
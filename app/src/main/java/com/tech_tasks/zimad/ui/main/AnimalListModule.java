package com.tech_tasks.zimad.ui.main;

import com.tech_tasks.zimad.data.AnimalEntry;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class AnimalListModule {

    @Provides
    AnimalListFragment provideListFragment() {
        return new AnimalListFragment();
    }

    @Provides
    List<AnimalEntry> provideDataList() {
        return new ArrayList<>();
    }

}

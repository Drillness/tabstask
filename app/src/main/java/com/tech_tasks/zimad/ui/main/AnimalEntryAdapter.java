package com.tech_tasks.zimad.ui.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tech_tasks.zimad.R;
import com.tech_tasks.zimad.base.BindableAdapter;
import com.tech_tasks.zimad.data.AnimalEntry;
import com.tech_tasks.zimad.databinding.AnimalListItemBinding;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

public class AnimalEntryAdapter extends RecyclerView.Adapter<AnimalEntryAdapter.ViewHolder>
        implements BindableAdapter<AnimalEntry> {

    private List<AnimalEntry> animalEntryList = new ArrayList<>();
    private AnimalListViewModel viewModel;

    AnimalEntryAdapter(AnimalListFragment frag) {
        viewModel = ViewModelProviders.of(frag).get(AnimalListViewModel.class);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AnimalListItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.animal_list_item,
                parent, false);

        return new AnimalEntryAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(animalEntryList.get(position));
    }

    @Override
    public int getItemCount() {
        return animalEntryList.size();
    }

    @Override
    public void setData(List<AnimalEntry> data) {
        this.animalEntryList = data;
        notifyDataSetChanged();
    }

    protected AnimalListViewModel getViewModel() {
        return viewModel;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private AnimalListItemBinding binding;

        ViewHolder(@NonNull AnimalListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindData(AnimalEntry animalEntry) {
            binding.setAnimalEntry(animalEntry);
            binding.getRoot().setOnClickListener((v) -> {
                getViewModel().onItemSelected(animalEntry);
            });
        }
    }

}

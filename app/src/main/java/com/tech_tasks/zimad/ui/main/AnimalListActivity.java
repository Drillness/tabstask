package com.tech_tasks.zimad.ui.main;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.tech_tasks.zimad.R;
import com.tech_tasks.zimad.TestApplication;
import com.tech_tasks.zimad.base.BaseActivity;
import com.tech_tasks.zimad.data.AnimalEntry;
import com.tech_tasks.zimad.ui.details.DetailsActivity;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Lazy;


public class AnimalListActivity extends BaseActivity implements AnimalListNavigator {

    private static final String CURRENT_TAB_KEY = "CURRENT_TAB_KEY";
    private static final String DOG_REQUEST = "dog";
    private static final String CAT_REQUEST = "cat";
    private static final int CAT_TAB = 0;
    private static final int DOG_TAB = 1;

    @Inject
    Lazy<AnimalListFragment> fragmentForCats;

    @Inject
    Lazy<AnimalListFragment> fragmentForDogs;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tabs)
    TabLayout tabs;

    private int currentSelectedTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TestApplication.getAppComponent().inject(this);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        initializeTab(tabs);

        if (savedInstanceState != null)
            currentSelectedTab = savedInstanceState.getInt(CURRENT_TAB_KEY);
        else
            currentSelectedTab = CAT_TAB;

        FragmentManager manager = getSupportFragmentManager();
        AnimalListFragment fragment = (AnimalListFragment) manager.findFragmentById(R.id.mainContent);

        chooseFragment(currentSelectedTab);

        tabs.selectTab(tabs.getTabAt(currentSelectedTab));
    }

    public void initializeTab(TabLayout tabLayout) {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.cat_tab));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.dog_tab));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (currentSelectedTab != tab.getPosition())
                    chooseFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Not needed
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // Not needed
            }
        });
    }

    private void chooseFragment(int tagNumber) {
        currentSelectedTab = tagNumber;
        AnimalListFragment fragment;
        String request;
        switch (tagNumber) {
            case CAT_TAB:
                fragment = fragmentForCats.get();
                request = CAT_REQUEST;
                break;
            case DOG_TAB:
                fragment = fragmentForDogs.get();
                request = DOG_REQUEST;
                break;
            default:
                fragment = fragmentForCats.get();
                request = CAT_REQUEST;
                break;
        }
        replaceFragment(fragment);
        fragment.getViewModel().setNavigator(this);
        fragment.getViewModel().makeRequest(request);
    }

    private void replaceFragment(Fragment frag) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.mainContent, frag)
                .commitNow();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_TAB_KEY, currentSelectedTab);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_animal_list;
    }

    @Override
    public void openDetails(AnimalEntry animalEntry) {
        Intent bookDetailsIntent = new Intent(this, DetailsActivity.class);
        bookDetailsIntent.putExtra(DetailsActivity.ANIMAL_ENTRY_INTENT_KEY, animalEntry);
        startActivity(bookDetailsIntent);
    }

}

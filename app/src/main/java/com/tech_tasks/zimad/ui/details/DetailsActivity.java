package com.tech_tasks.zimad.ui.details;

import android.os.Bundle;
import android.view.MenuItem;

import com.tech_tasks.zimad.R;
import com.tech_tasks.zimad.TestApplication;
import com.tech_tasks.zimad.base.BaseActivity;
import com.tech_tasks.zimad.data.AnimalEntry;

import javax.inject.Inject;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class DetailsActivity extends BaseActivity implements DetailsNavigator {
    public static final String ANIMAL_ENTRY_INTENT_KEY = "data";

    @Inject
    DetailsFragment detailsFragment;

    private DetailsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.detailed_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TestApplication.getAppComponent().inject(this);

        Bundle passedData = getIntent().getExtras();
        final AnimalEntry animalEntry = passedData != null
                ? passedData.getParcelable(ANIMAL_ENTRY_INTENT_KEY) : null;

        FragmentManager fragmentManager = getSupportFragmentManager();
        DetailsFragment fragment = (DetailsFragment) fragmentManager
                .findFragmentById(R.id.detailsFrame);

        if (fragment == null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.detailsFrame, detailsFragment);
            transaction.commit();
        }

        viewModel = getViewModel(DetailsViewModel.class);
        viewModel.setInspectedAnimalEntry(animalEntry);
        viewModel.setNavigator(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_details;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void goBackToList() {
        finish();
    }
}

package com.tech_tasks.zimad.ui.details;

import com.tech_tasks.zimad.BR;
import com.tech_tasks.zimad.base.BaseViewModel;
import com.tech_tasks.zimad.data.AnimalEntry;

import androidx.annotation.Nullable;
import androidx.databinding.Bindable;


public class DetailsViewModel extends BaseViewModel<DetailsNavigator> {

    @Nullable
    private AnimalEntry inspectedAnimalEntry;

    @Nullable
    @Bindable
    public AnimalEntry getInspectedAnimalEntry() {
        return inspectedAnimalEntry;
    }

    void setInspectedAnimalEntry(@Nullable AnimalEntry inspectedAnimalEntry) {
        this.inspectedAnimalEntry = inspectedAnimalEntry;
        notifyPropertyChanged(BR.inspectedAnimalEntry);
    }

    @Override
    public void start() {

    }

}
